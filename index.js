const express = require('express');
const path = require('path');
const fs = require('fs')
const app = express();
const port = 8080

fs.readFile('./files/file', 'utf8', (err, date) => {
    console.log(date)
})

app.use(express.json())

app.post('/api/files', (req, res) => {
    const filename = req.body.filename
    const content = req.body.content
    if (typeof filename == "undefined") {
        res.status(400).json({"message": "Please specify 'filename' parameter"});
    } else if (typeof content == "undefined") {
        res.status(400).json({"message": "Please specify 'content' parameter"});
    } else {
        fs.writeFile(`./files/${filename}`, content, function (err) {
            if (err) throw err;
            console.log('File is created successfully.');
        });
        res.json({"message": "File created successfully"});
    }
})

app.get('/api/files', (req, res) => {
    let fileArray = fs.readdirSync('./files', (err, files) => {
        return files.map(file => file)
    });
    res.json({"message": "Success", "files": fileArray});
})

app.get('/api/files/:filename', (req, res) => {
    const filename = req.params.filename
    let fileArray = fs.readdirSync('./files', (err, files) => {
        return files.map(file => file)
    });
    if (arrayContains(filename, fileArray)) {
        console.log("Contains")

        const data = fs.readFileSync(`./files/${filename}`,
            {encoding: 'utf8', flag: 'r'});
        console.log(data);
        let date = fs.statSync(`./files/${filename}`, (err, date) => {
            // TODO: implement
        });
        res.json({
            "message": "Success",
            "filename": filename,
            "content": data,
            "extension": getFileExtension(filename),
            "uploadedDate": date.birthtime
        })
    } else {
        res.status(400).json({
            "message": `No file with '${filename}' filename found`
        })
    }

})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

function arrayContains(needle, arrhaystack) {
    return (arrhaystack.indexOf(needle) > -1);
}

function getFileExtension(filename) {
    const extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length) || filename;
    return extension;
}

